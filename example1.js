"use strict";

class exampleDL extends dynamicLocale{

    constructor(){	
	super();
    }

    init_definitions(){
	var locales_dict = {"en" : "English",
			    "es" : "Español",
			    "fr" : "Français"};
	
	this.declare_available_locales(locales_dict);
	this.define_language_selector("#lang_select");
	
    }

    locale_path(locale){
	return `example_po/${locale}/example.json`;
    }

    
}

var p1 = document.querySelector("#p1");
var p2 = document.querySelector("#p2");
var p3 = document.querySelector("#p3");
var p5 = document.querySelector("#p5");
var p6 = document.querySelector("#p6");

var label=document.querySelector("#label");
var number=document.querySelector("#number");

var Napples=document.querySelector("#Napples");

var example = new exampleDL();

example.add_display_message(p1, ()=> example.i18n.gettext("Paragraph one"));
example.add_display_message(p2, ()=> example.i18n.gettext("Paragraph two"));
example.add_display_message(p3, ()=> example.i18n.gettext("Paragraph three"));

example.add_display_message(p5, ()=> example.i18n.ngettext("There is one sentence","There are two sentences",1));
example.add_display_message(p6, ()=> example.i18n.ngettext("There is one sentence","There are two sentences",2));

example.add_display_message(label, ()=> example.i18n.gettext("How many apples:"));
example.add_display_message(Napples, ()=> example.i18n.ngettext("There is %1 apple","There are %1 apples",number.value,number.value));

number.onchange=()=>{
    example.display_messages();
}
