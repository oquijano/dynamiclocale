# dynamicLocale

JavaScript library for changing the language of elements in a website
after some event. It works on top of
[gettext.js](https://guillaumepotier.github.io/gettext.js/) by
creating a class that keeps track of HTML elements whose contents
should be updated when there is a language change.


## Installation

In your HTML code you need to first load `gettext.js` (taken from
[gettext.js](https://guillaumepotier.github.io/gettext.js/) then load
`dynamiclocale.js`.

```html
<script src="gettext.js"></script>
<script src="dynamiclocale.js"></script>
```

All the other files in the repository are examples about the use of
dynamicLocale.

## Usage

You need to extend the dynamicLocale class and:

* In the constructor method you should call `super()`.

* Add a `init_definitions()` methods in which you call the method
  `declare_available_locales()` which receives a dictionary whose keys
  are locale names and the corresponding fields should be the name of
  the language (the one that is shown to the user in the selector)
  that corresponds to that locale. If you want to display the language
  selector you should also call `this.define_language_selector()` in
  this method.

* Add a `locale_path()` method. It should receive a locale name and
  return the path of a json file with the translations for that
  locale.
  
* Optionally add a method `on_language_change()` which will be called
  when the language is changed.
  
After instantiating your class you tag HTML elements for translation
using the method `add_display_message()` whose first argument is the
element and the second one is a thunk (a function that takes no
arguments) containing the
[gettext.js](https://guillaumepotier.github.io/gettext.js/) function
to run. You can look at how it is done in the provided examples.

If you want to reshow all the messages (for instance to update to
manage a plural) you can use the method `display_messages()`. There is
an example of this in the first provided example.

  
### Instantiating the dynamicLocale class

When `dynamicLocale` is instantiated (or super is called from a child
class), it can received two optional arguments

* **language_input_selector** : If provided, it should be the query id
  (e.g. "#lang") of a hidden input html element whose value is the
  first locale to use when the page is loaded. If not present the
  default locale is used.
  
* **default_locale** : The default locale to use, this is, the locale
  of the strings that are used with `i18n.gettext()`. If not provided
  `"en"` is assumed.


## Examples

Two examples are provided¸ both of them use the same translation files
(the example_po folder) with three languages available, english,
spanish and french.

For the first example you need to serve the files *example1.html*,
*example1.js* and the *example_po* folder. Similarly, the second
example consists of the files *example2.html*, *example2.js* and the
*example_po* folder.

In both examples there are four sentences and a button to change
language. There is one phrase that is not translated since
`add_display_message()` is not used for that element.

In the second example the optional argument `language_input_selector`
is used to set Spanish as the language that is shown after the page
loads and it also defines the method `on_language_change()` to show an
alert every time the language is changed.


<!--  LocalWords:  dynamicLocale
 -->
